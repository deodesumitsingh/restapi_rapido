from flask import Flask

import models
from resources.users import users_api
from resources.drivers import drivers_api
from resources.feedbacks import feedbacks_api


DEBUG   = True
PORT    = 5000
HOST    = '0.0.0.0'

app = Flask(__name__)
app.register_blueprint(users_api)
app.register_blueprint(drivers_api)
app.register_blueprint(feedbacks_api)

@app.route('/')
def index():
    return "Hello, World"

if __name__ == '__main__':
    models.initialize()
    app.run(debug = DEBUG, port = PORT, host = HOST)
