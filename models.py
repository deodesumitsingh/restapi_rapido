import datetime, random

from peewee import *

DATABASE = SqliteDatabase('rapido.db')

class User(Model):
    firstName       = CharField(max_length = 120)
    lastName        = CharField(max_length = 120)
    joinedDate      = DateTimeField(default = datetime.datetime.now)

    class Meta:
        database = DATABASE

class Driver(Model):
    firstName       = CharField(max_length = 120)
    lastName        = CharField(max_length = 120)
    joinedDate      = DateTimeField(default = datetime.datetime.now)

    class Meta:
        database = DATABASE

class FeedBack(Model):
    fromUser    = ForeignKeyField(User, related_name = "user_list")
    toDriver    = ForeignKeyField(Driver, related_name = "driver_list")
    tripDate    = DateTimeField(default = datetime.datetime.now)
    rating      = IntegerField(default = random.randrange(1, 6))

    class Meta:
        database = DATABASE

def populate_data():
    firstNameUser       = ["Varsha", "Nishith", "Antony", "Deepshika", "Himanshu", "Ansh"]
    lastNameUser        = ["Kumar", "Sharma", "Chopra", "Ahlawat", "Singh", "Patel"]

    for _ in range(50):
        User.create(firstName = firstNameUser[random.randrange(0, 6)],
                    lastName = lastNameUser[random.randrange(0, 6)])

    firstNameDriver = ["Kanay", "Deepak", "Mohit", "Suhird", "Navin", "Puneet", "Ravindra"]
    lastNameDriver  = ["Pujara", "Khan", "Kulkarni", "Raman", "Sowle", "Verma", "Jino"]

    for _ in range(50):
        Driver.create(firstName = firstNameDriver[random.randrange(0, 7)],
                       lastName = lastNameDriver[random.randrange(0, 7)])

    for _ in range(500):
        user = User.get(User.id == random.randrange(1, 51))
        driver = Driver.get(Driver.id == random.randrange(1, 51))
        FeedBack.create(fromUser = user, toDriver = driver, rating = random.randrange(1, 6))

def initialize():
    DATABASE.connect()
    #To over come database lock situation
    DATABASE.drop_tables([User, Driver, FeedBack], safe = True)

    DATABASE.create_tables([User, Driver, FeedBack], safe = False)

    #To delete previous data, to avoid data lock in database(Only happens while running locally)
    User.delete().execute()
    Driver.delete().execute()
    FeedBack.delete().execute()

    # In order to create dummy data
    populate_data()

    DATABASE.close()
