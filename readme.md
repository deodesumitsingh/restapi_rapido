### Submission towards Rapido

Technologies that been used are :-  
* Python :- _As core programming language_  
* Flask, Flask_Restful :- Underlying **framework** and its package  
* Peewee :- **ORM** to work with relational _databases_  
* Sqlite :- Database which is used;  

#### Database Tables used
1. User
 firstName  = CharField(max_length = 120) # first name of user  
 lastName   = CharField(max_length = 120) # last name of user  
 joinedDAte = DateTimeField(default = datetime.datetime.now) # time when user signed up  

2. Driver
 firstName  = CharField(max_length = 120) # first name of driver  
 lastName   = CharField(max_length = 120) # last name of driver  
 joinedDAte = DateTimeField(default = datetime.datetime.now) # time when driver joined  

3. FeedBack
 fromUser    = ForeignKeyField(User, related_name = "user_list") #r eference to user which take ride  
 toDriver    = ForeignKeyField(Driver, related_name = "driver_list") # ref to driver who drivers  
 tripDate    = DateTimeField(default = datetime.datetime.now) # date on which trip happened  
 rating      = IntegerField(default = random.randrange(1, 6)) # rating in between [1, 5]  

#### Resources EndPoints
There are basically 3 resources end points are given which are stated below
1. For User
 /api/v1/users # This will fetch all the users records [**GET**, **POST**]  
 /api/v1/users/<int:id> # This will fetch individual user record [**GET**, **PUT**]  

2. For Drivers
 /api/v1/drivers # This will fetch all the drivers records [**GET**, **POST**]  
 /api/v1/dirvers/<int:id> # This will fetch individual driver record [**GET**, **PUT**]  
 /api/v1/drivers/<ind:id>/topreviews # This will fetch individual driver top 5 ratings  

3. For FeedBack
 /api/v1/feedbacks # This will fetch all the feedbacks includes user as well as drivers name, and specified rating [**GET**, **POST**]  
 /api/v1/feedbacks/<int:id> # This will fetch individual feedback [**GET**]  

##### Installation Process
In order to install above project I highly recommend to user **virtualenv**, if you don't know you can get virtualenv [from here](https://virtualenv.pypa.io/en/stable/installation/)  
> $ git clone https://bitbucket.org/deodesumitsingh/restapi_rapido/src/master/  
> $ cd master   
> $ pip install virtualenv  
> $ virtualenv env  
> $ source env/bin/activate (for mac/ linux)  
> $ cd env/Scripts (for windows)  
> $ activate (for windows)  
> (env) $ **This means your virtual enviroment is active now**  
> (env) $ pip install -r requirements.txt  
> (env) $ python app.py  
> ** Serving Flask app "app" (lazy loading) # this means app is running now  

_Go to_ localhost:5000/api/v1/users to get all the users information, similiarliry to all the endpoints which specified above.  

**Use [_Postman_](https://www.getpostman.com) for API's action**  
