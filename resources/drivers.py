from flask import Blueprint, jsonify
from flask_restful import Api, Resource, abort, reqparse, fields, inputs, marshal, marshal_with, url_for

from peewee import *

import models
import resources.feedbacks
import resources.get_or_404 as get_or_404

driver_fields = {
    "id"            : fields.Integer,
    "firstName"     : fields.String,
    "lastName"      : fields.String,
    "totalTrips"    : fields.Integer
}


def add_trips(drivers):
    drivers.totalTrips = len([url_for("resources.feedbacks.feedback",
                            id = driver.id) for driver in drivers.driver_list])
    return drivers

class DriverList(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "firstName",
            required    = True,
            type        = str,
            help        = "Please provide FIRSTNAME for driver",
            location    = ["forms", "json"],
        )
        self.reqparse.add_argument(
            "lastName",
            required    = True,
            type        = str,
            help        = "Please provide LASTNAME for driver",
            location    = ["forms", "json"]
        )
        super().__init__()

    def get(self):
        drivers = models.Driver.select()
        drivers = [marshal(add_trips(driver), driver_fields)
                    for driver in drivers]
        return {"drivers": drivers}

    @marshal_with(driver_fields)
    def post(self):
        args = self.reqparse.parse_args()
        try:
            if args["firstName"] == "" or args["lastName"] == "":
                raise(IntegrityError)
            driver = models.Driver.create(**args)
        except IntegrityError:
            abort(404, message = "Empty string is not allowed")
        else:
            return (add_trips(driver),
                    201, {"Location": url_for("resources.drivers.driver",
                            id = driver.id)})


class Driver(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "firstName",
            required    = True,
            type        = str,
            help        = "Please provide FIRSTNAME for driver",
            location    = ["forms", "json"],
        )
        self.reqparse.add_argument(
            "lastName",
            required    = True,
            type        = str,
            help        = "Please provide LASTNAME for driver",
            location    = ["forms", "json"]
        )
        super().__init__()

    @marshal_with(driver_fields)
    def get(self, id):
        driver = get_or_404.driver_or_404(id)
        return add_trips(driver)

    @marshal_with(driver_fields)
    def put(self, id):
        driver = get_or_404.driver_or_404(id)
        try:
            args = self.reqparse.parse_args()
            if args["firstName"] == "" or args["lastName"] == "":
                raise(IntegrityError)
        except IntegrityError:
            abort(400, message = "Empty string is not allowed")
        else:
            query = models.Driver.update(**args).where(models.Driver.id == id)
            query.execute()
            #return user
            driver = models.Driver.get(models.Driver.id == id)
            return (add_trips(driver),
                    200, { "Location" : url_for("resources.drivers.driver",
                            id = driver.id)})

    #We cannot delete driver so commenting other delete functionality
    '''@marshal_with(driver_fields)
    def delete(self, id):
        driver = get_or_404.driver_or_404(id)
        query = models.Driver.delete().where(models.Driver.id == driver.id)
        query.execute()
        return (' ', 204, {"Location": url_for("resources.drivers.drivers")})'''


class TopReview(Resource):
    def get(self, id):
        driver  = get_or_404.driver_or_404(id)
        query   = models.FeedBack.select().where(models.FeedBack.toDriver ==
                    id).order_by(models.FeedBack.rating.desc()).limit(5)
        feedbacks = []
        for feedback in query:
            feedbacks.append(marshal(resources.feedbacks.
                            add_user_driver_name(feedback),
                            resources.feedbacks.feedback_fields))
        return {"Top Rating": feedbacks}

drivers_api = Blueprint("resources.drivers", __name__)

api = Api(drivers_api)

api.add_resource(
    DriverList,
    '/api/v1/drivers',
    endpoint = "drivers"
)

api.add_resource(
    Driver,
    '/api/v1/drivers/<int:id>',
    endpoint = "driver"
)

api.add_resource(
    TopReview,
    '/api/v1/drivers/<int:id>/topreviews',
    endpoint = "topreviews"
)
