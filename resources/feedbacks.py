from flask import Blueprint, jsonify
from flask_restful import (Api, Resource, reqparse, url_for,
                            abort, fields, inputs, reqparse,
                            marshal, marshal_with)

import random

import models
import resources.get_or_404 as get_or_404

feedback_fields = {
    "id"        : fields.String,
    "User"      : fields.String,
    "Driver"    : fields.String,
    "rating"    : fields.Integer
}

def add_driver_name(feedbacks):
    driverName = ''
    try:
        driver = models.Driver.get(models.Driver.id == feedbacks.toDriver)
    except models.Driver.DoesNotExist:
        pass
    else:
        driverName = driver.firstName + ' ' + driver.lastName

    feedbacks.Driver = driverName
    return feedbacks

def add_user_driver_name(feedbacks):
    feedbacks = add_driver_name(feedbacks)
    userName = ''
    try:
        user = models.User.get(models.User.id == feedbacks.fromUser)
    except models.User.DoesNotExist:
        pass
    else:
        userName = user.firstName + ' ' + user.lastName
    feedbacks.User = userName
    return feedbacks

class FeedBackList(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "fromUser",
            required = True,
            type = inputs.positive,
            help = "Please enter positive ID of user",
            location = ["form", "json"]
        )
        self.reqparse.add_argument(
            "toDriver",
            required = True,
            type = inputs.positive,
            help = "Please enter positive ID of driver",
            location = ["form", "json"]
        )
        self.reqparse.add_argument(
            "rating",
            required = False,
            default = random.randrange(1, 6),
            type = inputs.int_range(1, 5),
            help = "Pleaes enter number in between [1, 5]",
            location = ["form", "json"]
        )

    def get(self):
        query = models.FeedBack.select()
        feedbacks = []
        for feedback in query:
            feedback = add_user_driver_name(feedback)
            if feedback.Driver != '' and feedback.User != '':
                feedbacks.append(marshal(feedback, feedback_fields))
        return {"feedbacks": feedbacks}

    @marshal_with(feedback_fields)
    def post(self):
        args = self.reqparse.parse_args()
        get_or_404.user_or_404(args["fromUser"])
        get_or_404.driver_or_404(args["toDriver"])
        feedback = models.FeedBack.create(**args)
        return add_user_driver_name(feedback)

class FeedBack(Resource):

    @marshal_with(feedback_fields)
    def get(self, id):
        feedback = get_or_404.feedback_or_404(id)
        return add_user_driver_name(feedback)

    #No need for put in single FeedBack, because user cannot update it later
    #No need for delte in feedback, because it's cannot be deleted

feedbacks_api = Blueprint("resources.feedbacks", __name__)

api = Api(feedbacks_api)

api.add_resource(
    FeedBackList,
    '/api/v1/feedbacks',
    endpoint = "feedbacks"
)

api.add_resource(
    FeedBack,
    '/api/v1/feedbacks/<int:id>',
    endpoint = "feedback"
)
