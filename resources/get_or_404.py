import models

from flask_restful import abort

def driver_or_404(driverID):
    try:
        driver = models.Driver.get(models.Driver.id == driverID)
    except models.Driver.DoesNotExist:
        abort(404, message = "Driver ID {} not found".format(driverID))
    else:
        return driver

def user_or_404(userID):
    try:
        user = models.User.get(models.User.id == userID)
    except models.User.DoesNotExist:
        abort(404, message = "User ID {} not found".format(userID))
    else:
        return user

def feedback_or_404(feedbackID):
    try:
        feedback = models.FeedBack.get(models.FeedBack.id == feedbackID)
    except models.FeedBack.DoesNotExist:
        abort(404, message = "FeedBack ID {} not found".format(feedbackID))
    else:
        return feedback
