from flask import Blueprint, jsonify
from flask_restful import (Api, Resource, reqparse, fields, inputs, marshal,
                            marshal_with, url_for, abort)

from peewee import *

import models
import resources.get_or_404 as get_or_404

user_fields = {
    "id"            : fields.Integer,
    "firstName"     : fields.String,
    "lastName"      : fields.String,
    "totalRides"    : fields.Integer
}

def add_rides(users):
    users.totalRides = len([(url_for("resources.feedbacks.feedback",
                            id = feedback.id))for feedback in users.user_list])
    return users

class UserList(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "firstName",
            required    = True,
            help        = "Please enter FIRSTNAME of user",
            location    = ["form", "json"],
            type        = str
        )
        self.reqparse.add_argument(
            "lastName",
            required    = True,
            help        = "Pleaes enter LASTNAME of user",
            location    = ["form", "json"],
            type        = str
        )
        super().__init__()

    def get(self):
        querys  = models.User.select()
        users   = [marshal(add_rides(users), user_fields) for users in querys]
        return {"users" : users}

    @marshal_with(user_fields)
    def post(self):
        args = self.reqparse.parse_args()
        try:
            if args["firstName"] == "" or args["lastName"] == "":
                raise(IntegrityError)
            user = models.User.create(**args)
        except IntegrityError:
            abort(404, message = "Empty string is not allowed")
        else:
            return (add_rides(user), 201,
            {"Location" : url_for("resources.users.user", id = user.id)})

class User(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument(
            "firstName",
            required    = True,
            help        = "Please enter FIRSTNAME of user",
            location    = ["form", "json"],
            type        = str
        )
        self.reqparse.add_argument(
            "lastName",
            required    = True,
            help        = "Pleaes enter LASTNAME of user",
            location    = ["form", "json"],
            type        = str
        )
        super().__init__()

    @marshal_with(user_fields)
    def get(self, id):
        user = get_or_404.user_or_404(id)
        return add_rides(user)

    @marshal_with(user_fields)
    def put(self, id):
        user = get_or_404.user_or_404(id)
        try:
            args = self.reqparse.parse_args()
            if args["firstName"] == "" or args["lastName"] == "":
                raise(IntegrityError)
        except IntegrityError:
            abort(400, message = "Empty string is not allowed")
        else:
            query = models.User.update(**args).where(models.User.id == id)
            query.execute()
            #return user
            user = models.User.get(models.User.id == id)
            return (add_rides(user), 200, {
            "Location" : url_for("resources.users.user", id = user.id)})

    #We cannot delete user from DB, to commenting delete
    '''def delete(self, id):
        user = get_or_404.user_or_404(id)
        query = models.User.delete().where(models.User.id == user.id)
        query.execute()
        return (' ', 204, {"Location": url_for("resources.users.users")})'''

users_api = Blueprint("resources.users", __name__)

api = Api(users_api)

api.add_resource(
    UserList,
    '/api/v1/users',
    endpoint = "users"
)

api.add_resource(
    User,
    '/api/v1/users/<int:id>',
    endpoint = "user"
)
